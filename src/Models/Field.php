<?php namespace float\formBuilder\Models;

use Lang;

class Field{

	public $container = '<div class="col-md-12">
							<div class="form-group">
								#label#
								<div class="col-md-9">
									#field#
								</div>
							</div>
						</div>';
	
	public $default_label_options = ['class' => 'col-md-2 control-label'];
	
	public static $default_options = [
		'class' => 'form-control'
	];
	
	//Auto detection type by name
	public static $types = [
		'password' => ['password', 'confirm_password', 'password_confirm', 'pass', 'senha', 'palavra-passe'],
		'email' => ['email', 'e-mail'],
		'file' => ['file', 'ficheiro', 'imagem', 'thumbnail','thumbnail2','video', 'image', 'image2','image3','logo', 'logo_footer'],
		'checkbox' => ['cbox', 'checkbox'],
		'radio' => ['radio', 'rad'],
		'number' => ['number'],
		'date' => ['date', 'birthdate', 'data_nascimento'],
		'select' => ['select', 'group'],
		'selectRange' => ['selectRange'],
		'selectMonth' => ['selectMonth'],
		'button' => ['reset', 'cancel'],
		'submit' => ['submit', 'confirm']
	];
	
	//has placeholder
	public static $has_placeholder = ['password', 'email', 'number', 'date', 'select', 'selectRange', 'selectMonth', 'text'];
	public static $dont_have_container = ['submit', 'button', 'reset'];
	
	public $predefined = [];
	public $multi;
	public $name;
	public $type;
	public $options;
	public $has_container = true;
	public $languageContainer;
	public $is_edit;
	
	public function __construct($multi, $is_edit = false){
	
		$this->multi = $multi;
	
		$this->is_edit = $is_edit;
		
		if(!is_array($multi))
			$this->buildFromName();
		else
			$this->buildFromArray();
		
		$this->setPlaceholder();
		
		$this->setContainer();
		
		$this->setLanguage();
	}
	
	public function getPredefined(){

		if(in_array($this->name, ['submit', 'reset', 'cancel']))
			switch($this->name){
				case "submit":
					$this->predefined['submit'] = [
						"content" => "<i class=\"fa fa-check\"></i> " . ($this->is_edit ? Lang::get("backoffice/main.edit") : Lang::get("backoffice/main.create")),
						"options" => [
							"class" => "btn btn-sm btn-success",
							"type" => "submit"
						]
					];
					break;
				case "reset":
					$this->predefined['reset'] = [
						"content" => "<i class=\"fa fa-undo\"></i> " . Lang::get("backoffice/main.reset"),
						"options" => [
							"class" => "btn btn-sm btn-default",
							"type" => "reset"
						]
					];
					break;
				case "cancel":
					$this->predefined['cancel'] = [
						"content" => "<i class=\"fa fa-ban\"></i> " . Lang::get("backoffice/main.cancel"),
						"options" => [
							"class" => "btn btn-sm btn-warning close_popup",
							"type" => "reset"
						]
					];
					break;	
			}
	}
	
	public function buildFromName(){
		
		$this->name = $this->multi;

		$this->getPredefined();
		
		if(isset($this->predefined[$this->multi])){	
			$this->content = $this->predefined[$this->multi]['content'];
			$this->type = $this->predefined[$this->multi]['options']['type'];
			$this->options = $this->predefined[$this->multi]['options'];
		}	
	}
	
	public function buildFromArray(){
		$this->name = $this->multi['name'];
		$this->options = isset($this->multi['options']) ? array_merge(static::$default_options, $this->multi['options']) : static::$default_options;
		$this->value = isset($this->multi['value']) ? $this->multi['value'] : null;
		//Set type if custom type wasnt set
		if(!empty($this->options['type']))
			$this->type = $this->options['type'];
		foreach (static::$types as $type => $array){
			if(in_array($this->name, $array)){
				$this->type = $type;
				break;
			}
		}	
		//Check if type was found, if not set text as default
		if(empty($this->type))
			$this->type = "text";
		
		//Add disabled attribute to created at and updated_at fields
		if(in_array($this->name, ['created_at', 'updated_at']))
			$this->options['disabled'] = "disabled";
		
		//Set id if it wasn't set
		if(empty($this->options['id']))
			$this->options['id'] = $this->name;
	}
	
	public function setPlaceholder(){
		//Set placeholder field for the field if it applies
		if(in_array($this->type, static::$has_placeholder) && !isset($this->options['no-placeholder'])){
			$this->options['placeholder'] = Lang::get("backoffice/main.{$this->name}");
		}
	}
	
	public function setContainer(){
		//Set placeholder field for the field if it applies
		if(in_array($this->type, static::$dont_have_container)){
			$this->has_container = false;
		}
		else{
			if(!empty($this->options['container'])){
				$this->container = $this->options['container'];
			}
		}
	}
	
	public function setLanguage(){
		$this->languageContainer = $this->is_edit ? 
									'<div class="col-md-1">
										<a href="'.\URL::to('backoffice/languages/item/'.(isset($this->multi['table']) ? $this->multi['table'] : 'pages'  ).'/'.$this->name.'/'.$this->is_edit).'" class="iframe"><button class="btn btn-sm btn-default" type="button"><span class="glyphicon glyphicon-flag"></span></button></a>
									</div>' : '';
	}
	
	public function output($formBuilder){
		$html = $this->html();
		
		//If item has container insert it inside
		if($this->has_container){
			return str_replace(
				['{{name}}', '#field#', '#label#'],
				[
					$this->name, 
					$html, 
					$formBuilder->label($this->name, Lang::get("backoffice/main.{$this->name}"), $this->default_label_options)
				],
				$this->container
			);
		}
		else{
			return $html;
		}
		
	}
	
	public function html(){
		$type = $this->type;
		$name = $this->name;
		//To get field values if present
		
		switch($type){
			case 'password':
				return FormFacade::password($name, $this->options);
			case 'email':
				return FormFacade::email($name, $this->options);
			case 'file':
				$html = FormFacade::file($name, $this->options);
				if($this->is_edit && $this->value != ''){
					$html .= '<div class="form-group2"> 
							<button type="button" class="btn btn-danger deleteField" data-field="'.$this->name.'"> '. Lang::get('backoffice/main.delete') .'</button> Archivo: '.$this->value.'
						</div>';
				}
				return $html;
			case 'checkbox':
			case 'radio':
				return FormFacade::$type($name, !empty($this->value) ? $this->value : 1, isset($this->options['checked']) ? true : false, $this->options);
			case 'text':
				if(isset($this->multi['table_type']) && $this->multi['table_type'] == 'tex')
					return FormFacade::textarea($name, !empty($this->value) ? $this->value : null, $this->options);
				else
					return FormFacade::text($name, !empty($this->value) ? $this->value : null, $this->options);
			case 'number':
				return FormFacade::number($name, isset($this->options['value']) ? $this->options['value'] : null, $this->options);
			case 'date':
				return FormFacade::date($name, isset($this->options['value']) ? $this->options['value'] : null, $this->options);
			case 'select':
				return FormFacade::select($name, $this->list, isset($this->selected) ? $this->selected : null, $this->options);
			case 'button':
			case 'submit':
			case 'reset':
			default:
				return FormFacade::button($this->content, $this->options);
		}
	}
}