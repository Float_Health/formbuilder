<?php namespace float\formBuilder\Models;

use float\formBuilder\Models\Field;

class Form{
	
	public $model_obj;
	public $custom;
	public $options = [];
	public $buttons = [];
	public $fields = [];
	
	public $config_array, $args, $field_container;
	
	public $excluded_fields = ['id', 'deleted_at'];
	
	public static $default_options = [
		'id' => 'default',
		'class' => 'fupload form-horizontal',
	];
	
	public static $buttons_outer_container = 
	'<!-- Buttons outer container -->
	<div class="col-md-12">
		<div class="form-group pull-right">
			#buttons#
		</div>
	</div>';
	
	public function __construct($model_obj, $custom){
		
		$this->model_obj = $model_obj;
		
		$this->custom = $custom;
		
		$this->getFields();
		
		$this->getButtons();
		
		$this->options = isset($this->custom['options']) ? array_merge(static::$options, $this->custom['options']) : static::$default_options;
		
	}
	
	public function getFields(){
		$options = [];
		//Add table fields through Formable Model function getAllFields()
		foreach($this->model_obj->getAllFields() as $name=>$field){
			$field['value'] = $this->model_obj->$name;
			$field['table'] = $this->model_obj->getTable();
			//Set custom options if they were set
			if(isset($this->custom['fields'][$name]['options'])){
				$options = $this->custom['fields']['elements'][$name]['options'];
			}	
			//Set custom shared field container if it was defined
			if(isset($this->custom['fields_container']) && empty($options['container'])){
				$options['container'] = $this->custom['fields_container'];
			}

			$field['options'] = $options;
			$this->fields[$name] = new Field($field, isset($this->model_obj->id) ? $this->model_obj->id : false);
		}
		//Unset unwanted fields
		$fields_to_exclude = isset($this->custom['excluded_fields']) ? array_merge($this->excluded_fields, $this->custom['excluded_fields']) : $this->excluded_fields;
		if(empty($this->model_obj->id))
			$fields_to_exclude = array_merge($fields_to_exclude, ['created_at', 'updated_at']);
		
		foreach($fields_to_exclude as $name)
			unset($this->fields[$name]);
		
	}
	
	public function getButtons(){
		
		if(isset($this->custom['buttons']['elements'])){
			//Set custom buttons
			foreach($this->custom['buttons']['elements'] as $name => $button){
				$this->buttons[$name] = new Field($button);
			}
		}
		//Set default buttons IF not defined in $this->custom['excluded_buttons']
		else{
			foreach(['submit', 'reset', 'cancel'] as $name){
				if(!isset($this->custom['excluded_buttons']) || (isset($this->custom['excluded_buttons']) && !in_array($name, $this->custom['excluded_buttons'])))
					$this->buttons[$name] = new Field($name, isset($this->model_obj->id) ? true : false);
			}
				
		}
		//Set custom buttons container	
		if(!empty($this->custom['buttons']['outer_container'])){
			static::$buttons_outer_container = $this->custom['buttons']['outer_container'];
		}	
	}
	
	
	public function build($formBuilder){
		 
		$html = FormFacade::open($this->options);
		
		foreach($this->fields as $item){			
			$html .= $item->output($formBuilder);
		}
		
		
		$button_html = '';
		
		foreach($this->buttons as $name => $button){
			$button_html .= $button->output($formBuilder);
		}
		
		$html .= str_replace(
					'#buttons#',
					$button_html,
					static::$buttons_outer_container
				);
		
		$html .= FormFacade::close();
		
		return $html;
	}
	
	public function buildScripts(){
		$html = '';
		//Scripts
		//Datepicker
		$html .= "<script type=\"text/javascript\">\n";
		
		foreach($this->fields as $field){
			
			if(isset($field->multi['table_type']) && $field->multi['table_type'] == 'dat')
				$html .= '$("#'.$field->name.'").datepicker({
					changeMonth: true,
					changeYear: true,
					dateFormat: "yy-mm-dd",
					yearRange: "-100:+0",
					dayNamesMin: ["Do", "Se", "Te", "Qa", "Qi", "Se", "Sa"],
					monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
					monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"]
				});';
			if(isset($field->type) && $field->type == 'file' && isset($this->model_obj->id))
				$html .= 
					'$(".deleteField").click(function(){
						id_file = '.$this->model_obj->id.';
						field = "'.$field->name.'";
						parent = $(this).parents(".form-group2");
						$.confirm({
							title: "Borrar archivo",
							content: "¿Está seguro que quiere eliminar?",
							confirmButtonClass: "btn-danger",
							confirm: function(button){
								$.ajax({
									type: "POST",
									url: "'.\URL::to("backoffice/news/deleteField").'",
									data: {
										"id": id_file,
										"field": field
									}
								}).done(function(data){
									console.log(data);
									if(data == true){
										parent.remove();
									}
								});
							},
							confirmButton: "Si",
							cancelButton: "No"
						});	
					});';
		}
		
		$html .= "</script>";
		
		return $html;
	}
	
}